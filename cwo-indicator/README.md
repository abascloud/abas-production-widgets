# abas-production-indicator-cwo-widget

## Description: 
- Displays the amount of critical work orders.
A work order is considered as critical when the deadline cannot be met.
- Results can be restricted by choosing a dispatcher, warehouse group, product and time period.
- The output is color coded (green/yellow/red) depending on the threshold set.
The threshold is the number of critical work orders tolerated.

## Settings: 
- Choosing a dispatcher by prefix search for the dispatcher's name (optional)
- Choosing a warehouse group by prefix search for the name of the warehouse group (optional)
- Choosing a product by prefix search for the product's name (optional)
- Defining a threshold as the number of critical work orders tolerated (mandatory, default: 20)
- Defining a start date
- Defining an end date (default: +5)
- Setting the Message Bus In and Message Bus Out for the communication with other components

## Properties:
The widget has some properties, you can/must set to use it.
- startDate: the start date used for requesting data to visualize; it has to be <= endDate && 0>= if endDate is defined
- endDate: the end date used for requesting data to visualize; it has to be >= startDate if startDate is defined

## Dependencies:
- The widget can work alone
- The widget can work together with the abas-production-detail-widget to show a detail table according to the request

## Testing
- for testing you need to install polymer-cli, web-component-tester, wct-istanbub and wct-xunit-reporter (npm -g install ...)
- to run the tests locally: wct --configFile=wct.conf.chrome.local.json
- also: polymer serve and open in browser "http://127.0.0.1:8000/components/abas-production-indicator-widget/test/"

