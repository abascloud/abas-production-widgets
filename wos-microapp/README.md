# abas-production-wos-widget

## Description: 
- Displays work order suggestions, which should have been released until yesterday (included) and all work order suggestions
- Results can be restricted by choosing a dispatcher, multiple indicator with different disponents can be set up
- Under the indicators a chart can be displayed by clicking 
- The indicators get sorted by risk
- The output can be color coded (green/red) by setting a threshold (default: 20)

## Settings: 
- Choosing a dispatcher by prefix search for the dispatcher's name (optional)
- Defining a threshold (mandatory, default: 20)
- Defining a start and end date for the bar chart visualization
- Defining a start and end date for requesting the data from the erp

## Properties:
The widget has some properties, you can/must set to use it
- threshold: the threshold for coloring the indicator number
- tenant: the erp tenant to use
- endDate: the last date, which will be visualized in the bar chart
- chartErpUrl: the url for the ERP, the bar chart should open when clicking on it

## Dependencies:
- The widget can work alone 

## Testing
- for testing you need to install polymer-cli, web-component-tester, wct-istanbub and wct-xunit-reporter (npm -g install ...)
- to run the tests locally: wct --configFile=wct.conf.chrome.local.json
- also: polymer serve and open in browser "http://127.0.0.1:8000/components/abas-production-indicator-widget/test/"

