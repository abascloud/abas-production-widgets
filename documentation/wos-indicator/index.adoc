= Dokumentation abas Work Order Suggestion Indicator-Widget
:page-layout: docs
:toc: left
:doctype: book
:icons: font
:copyright: abas Software GmbH

== Beschreibung & Funktionen
//Description of the widget and its features
Das Work Order Suggestion Indicator-Widget zeigt die Anzahl an Fertigungsvorschlägen an, die bis einschließlich gestern hätten freigegeben werden sollen. Die Ergebnisse können über die Auswahl eines Disponenten eingeschränkt werden. Die Ausgabe kann farbkodiert erfolgen (grün/rot), indem ein Schwellenwert definiert wird (Standardwert: 20).

Der Balken im Widget visualisiert die Beziehung zwischen verzögerten Fertigungsvorschlägen (grüner oder roter Balken, abhängig vom Schwellenwert), allen Umlagerungsvorschlägen (gesamter Balken) und dem Schwellenwert (schwarze Markierung).

Die Farbe des Kreises in der oberen rechten Ecke zeigt an, zu welcher Widget-Gruppe es gehört. Die Information zur Widget-Gruppe ist vor allem in Kombination mit dem Production Data View- und Chart-Widget erforderlich. Ein Klick auf das Work Order Suggestion Indicator-Widget versorgt diese Widgets mit zu visualisierenden Daten. 

== Anwendungsfälle
//List of use cases the widget can be used for
- Anzeigen aller verzögerten Fertigungsvorschläge verglichen mit einem Schwellenwert.
- Anzeigen der verzögerten Fertigungsvorschläge für einen bestimmten Disponenten.

== Beispiele
//Images of examples what the widget could look like
.Fertigungsvorschläge unterhalb des Schwellenwerts
image::wos_example1.jpg[Fertigungsvorschläge unterhalb des Schwellenwerts]

.Fertigungsvorschläge oberhalb des Schwellenwerts
image::wos_example2.jpg[Fertigungsvorschläge oberhalb des Schwellenwerts]


== Einstellungen
//Description of the settings dialog (e.g. necessary settings, optional settings, etc.)

.Allgemeine Einstellungen und Einstellungen Indikator
image::wos_settings1.jpg[Allgemeine Einstellungen und Einstellungen Indikator]
.Zeitraum und Widget-Gruppe
image::wos_settings2.jpg[Zeitraum und Widget-Gruppe]

=== Allgemein

==== Titel
Tragen Sie hier den Titel ein, den Sie im Widget anzeigen möchten.

==== abas ERP-Mandant
Sie können für die in diesem Widget verarbeiteten und angezeigten Daten einen Standardkontext festlegen, indem Sie einen abas ERP-Mandanten auswählen. Dieser Kontext kann über den URL-Parameter "mandant" überschrieben werden, sofern er nicht als "Fixiert" markiert ist.

==== Aktualisierungsintervall
Standardmäßig ist das Aktualisierungsintervall deaktiviert. Wenn Sie das Aktualisierungsintervall aktivieren, werden die Daten nach dem als Intervall angegebenen Zeitraum aktualisiert. Hinweis: Aktualisierungen in zu kurzen Intervallen können zu einer hohen Last auf der abas REST API führen.


=== Indikator

==== Schwellenwert
Tragen Sie hier die Anzahl an tolerierten verzögerten Fertigungsvorschlägen ein (Pflichtfeld, Standardwert: 20).

==== Disponent 
Wählen Sie hier einen Disponenten aus, um die Daten zu filtern (optional).


=== Zeitraum 

==== Startdatum/Enddatum in Tagen ab heute
Geben Sie hier den Zeitraum an, für den Sie Daten abfragen möchten. Sowohl das Start- als auch das Enddatum werden als Anzahl an Tagen in Bezug auf das Tagesdatum definiert. "-50" bedeutet 50 Tage in die Vergangenheit ab heute, "+50" bedeutet 50 Tage in die Zukunft ab heute.

==== Detailansicht - Startdatum/Enddatum in Tagen ab heute
Geben Sie hier den Zeitraum an, den Sie für eine detaillierte Anzeige im Säulendiagramm im Production Chart- oder Data View-Widget verwenden möchten. Sowohl das Start- als auch das Enddatum werden als Anzahl an Tagen in Bezug auf das Tagesdatum definiert. "-50" bedeutet 50 Tage in die Vergangenheit ab heute, "+50" bedeutet 50 Tage in die Zukunft ab heute.


=== Widget-Gruppe
Alle Fertigungs-Widgets kommunizieren miteinander innerhalb derselben Widget-Gruppe. Die Standard-Widget-Gruppe ist "default". Sie können die Widget-Gruppe, zu der ein Widget gehört, ändern, um ein Widget abzugrenzen oder um Widgets zu gruppieren.

