console.log("=== Check usage of locales.json ===");
// console.log(process.argv.slice(2));

const yargs = require('yargs/yargs')
const { hideBin } = require('yargs/helpers')
const argv = yargs(hideBin(process.argv)).argv

const fs = require('fs');
const path = require('path');

const LANG = argv.lang || "en";
console.log("Language: " + LANG);
console.log("===================================");

const DIR_TO_CHECK = path.resolve("./");
let localesKeys = Object.keys(JSON.parse(fs.readFileSync("locales.json"))[LANG]);
let foundKeys = {};
let regexes = [
    new RegExp(String.raw`(\[\[localize\(\")(.[^\"]*)(\"\)\]\])`, 'gm'),
    new RegExp(String.raw`(this\.localize\(\")(.[^\"]*)(\"\))`, 'gm'),
    new RegExp(String.raw`(\[\[localize\(\')(.[^\']*)(\'\)\]\])`, 'gm'),
    new RegExp(String.raw`(this\.localize\(\')(.[^\']*)(\'\))`, 'gm')
]

function traverseDir(dir) {
    fs.readdirSync(dir)
        .filter((file) => !file.startsWith('.'))
        .forEach(file => {
            let fullPath = path.join(dir, file);
            if (fs.lstatSync(fullPath).isDirectory()) {
                traverseDir(fullPath);
            } else {
                if (file.endsWith('.html') || file.endsWith('.js') || (file.endsWith('.json') && file !== 'locales.json')) {
                    checkFile(fullPath);
                }
            }
        });
}

function checkFile(filepath) {
    let content = fs.readFileSync(filepath).toString('utf-8');
    regexes.forEach(regex => {
        let matches = content.match(regex);
        if (matches != null && matches.length > 0) {
            matches.forEach(match => {
                //check if is double quote
                if ((match.match(/\"/g) || []).length) {
                    const key = match.split('"')[1];
                    foundKeys[key] = true;
                } else {
                    const key = match.split('\'')[1];
                    foundKeys[key] = true;
                }
            })
        }
    })
}

traverseDir(DIR_TO_CHECK);

let unusedKeys = 0;
let missingKeys = 0;
localesKeys.forEach(key => {
    if (foundKeys[key] == undefined) {
        console.log("Unused Key in locales: " + key);
        unusedKeys++;
    }
});

Object.keys(foundKeys).forEach(key => {
    if (!localesKeys.includes(key)) {
        console.log("Missing Key in locales: " + key);
        missingKeys++;
    }
})

if (unusedKeys > 0 || missingKeys > 0) {
    process.exit(1);
} else {
    console.log("All fine. :)");
}
