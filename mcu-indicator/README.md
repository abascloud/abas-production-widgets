# abas-production-indicator-mcu-widget

## Description: 
- Displays machine capacity utilization
- Results can be restricted by choosing a work order, a warehouse group or a department
- The threshold in percentage for coloring too less workload can be set

## Settings: 
- Choosing a work order by prefix search for the work order's name (optional)
- Choosing a warehouse group by prefix search for the warehouse group's name (optional)
- Choosing a department by prefix search for the department's name (optional)
- Defining a threshold for coloring too less workload (mandatory, default: 50)
- Setting the Message Bus In and Message Bus Out for the communication with other components
- Setting a start and end date for the visualization (default: from today (".") until "+7" days)

## Properties:
The widget has some properties, you can/must set to use it

- threshold: the threshold for coloring too less workload
- tenant: the erp tenant to use
- startDate: the start date used for requesting data to visualize; it has to be <= endDate and is required
- endDate: the end date used for requesting data to visualize; it has to be >= startDate and is required
- messageBusOut: the bus for the communication from this widget to others
- messageBusIn: the bus for the communication from other widgets to this one

## Dependencies:
- The widget can work alone 
- The widget can work together with the abas-production-chart to show a chart according to the request

## Testing
- for testing you need to install polymer-cli, web-component-tester, wct-istanbub and wct-xunit-reporter (npm -g install ...)
- to run the tests locally: wct --configFile=wct.conf.chrome.local.json
- also: polymer serve and open in browser "http://127.0.0.1:8000/components/abas-production-indicator-widget/test/"

