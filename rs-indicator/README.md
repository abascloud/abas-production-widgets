# abas-production-indicator-rs-widget

## Description: 
- Displays relocation suggestions, which should have been released until yesterday (included)
- Results can be restricted by choosing a dispatcher
- The output can be color coded (green/red) by setting a threshold (default: 20)

## Settings: 
- Choosing a dispatcher by prefix search for the dispatcher's name (optional)
- Defining a threshold (mandatory, default: 20)
- Setting the Message Bus In and Message Bus Out for the communication with other components
- Defining an end date for the bar chart visualization

## Properties:
The widget has some properties, you can/must set to use it

- threshold: the threshold for coloring the indicator number
- tenant: the erp tenant to use
- startDateErp: the start date used for requesting data to visualize; it is required but has to be <= -1
- endDateErp: the end date used for requesting data to visualize; it is not required but has to be >= -1
- startDate: the first date, which will be visualized in the bar chart; it is not required but has to be <= 0 
- endDate: the last date, which will be visualized in the bar chart; it is not required but has to be >= 0
- chartErpUrl: the url for the ERP, the bar chart should open when clicking on it
- messageBusOut: the bus for the communication from this widget to others
- messageBusIn: the bus for the communication from other widgets to this one

## Dependencies:
- The widget can work alone 
- The widget can work together with the abas-production-chart-widget to show a chart according to the request

## Testing
- for testing you need to install polymer-cli, web-component-tester, wct-istanbub and wct-xunit-reporter (npm -g install ...)
- to run the tests locally: wct --configFile=wct.conf.chrome.local.json
- also: polymer serve and open in browser "http://127.0.0.1:8000/components/abas-production-indicator-widget/test/"

