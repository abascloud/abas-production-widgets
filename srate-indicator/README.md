# abas-production-indicator-srate-widget

## Description: 
- Displays scrap rate for working centers
- Results can be restricted by choosing a department
- The output can be color coded (green/red) by setting a threshold for scrap rate and number of workinc centers above threshold

## Settings: 
- Choosing a department by prefix search for the department's name (optional)
- Defining a threshold for scrap rate (mandatory, default: 5)
- Defining a threshold for how many work centers are allowed to have a higher rate than the scrap rate which is set (mandatory, default: 2)
- Setting the Message Bus In and Message Bus Out for the communication with other components
- Defining an end date for the bar chart visualization

## Properties:
The widget has some properties, you can/must set to use it

- thresholdSrate: the threshold for the Scrap rate coloring the indicator number
- thresholdWorkCenters: the threshold for the number of work centers above threshold coloring the indicator number
- tenant: the erp tenant to use
- fromDate: the start date, which will be visualized in the bar chart
- messageBusOut: the bus for the communication from this widget to others
- messageBusIn: the bus for the communication from other widgets to this one

## Dependencies:
- The widget can work alone 
- The widget can work together with the abas-production-chart to show a chart according to the request

## Testing
- for testing you need to install polymer-cli, web-component-tester, wct-istanbub and wct-xunit-reporter (npm -g install ...)
- to run the tests locally: wct --configFile=wct.conf.chrome.local.json
- also: polymer serve and open in browser "http://127.0.0.1:8000/components/abas-production-indicator-widget/test/"
